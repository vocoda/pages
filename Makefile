JEKYLL?=bundle exec jekyll
DST=_site
MAKEFILES=Makefile $(wildcard *.mk)

FEATURED_IMGS=$(wildcard _img/posts/*jpg)

MINIMUM_ASSETS_IMGS = $(subst _img/posts/,assets/img/posts/,$(FEATURED_IMGS))

INSTALL_DEPS = jekyll
ifndef USE_CONVERT
INSTALL_DEPS+= gulp
endif

# Controls
.PHONY : print_help clean files
all : print_help

jekyll:
	@bundle install

gulp:
	@npm install gulp-cli # creates node_modules and puts binaries there
	@npm install natives #needed by gulp

## prepare          : install dependencies locally (define USE_CONVERT=1 to skip installing gulp)
prepare : $(INSTALL_DEPS)

ifndef USE_CONVERT
assets/img/posts/%.jpg : _img/posts/%.jpg
	@./node_modules/gulp/bin/gulp.js img
else
assets/img/posts/%.jpg : _img/posts/%.jpg
	@echo $< $(subst .jpg,,$@)
	@. script/prep_image.sh $< $(subst .jpg,,$@)
endif


## images           : run gulp/convert to create responsive clones of all images
##                    [define USE_CONVERT=1 to use imagemagick's convert to do the job
##                    e.g. run `USE_CONVERT=1 make images`]
images : ${MINIMUM_ASSETS_IMGS}


## serve            : run a local server.
serve :
	${JEKYLL} serve

## site             : build files but do not run a server.
site :
	${JEKYLL} build

## clean            : clean up junk files.
clean :
	@rm -rf ${DST}
	@rm -rf .sass-cache
	@rm -rf bin/__pycache__
	@find . -name .DS_Store -exec rm {} \;
	@find . -name '*~' -exec rm {} \;
	@find . -name '*.pyc' -exec rm {} \;

## print_help       : show all print_help.
print_help :
	@grep -h -E '^##' ${MAKEFILES} | sed -e 's/## //g'
