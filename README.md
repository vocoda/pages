[![pipeline status](https://gitlab.com/mlcdresden/pages/badges/master/pipeline.svg)](https://gitlab.com/mlcdresden/pages/commits/master)

---

# MLC Dresden Website

This is the repository to render the website of the MLC Dresden.

## How to render the website locally

This website is rendered using jekyll. It uses the [sleek theme](https://github.com/janczizikow/sleek). This theme requires some additional software to be installed. The following instructions assume that you are running macOS or a flavor of Linux and that you have the following installed:

- ruby including ruby development packages
- gem, the ruby package manager
- nodejs
- npm, the nodejs package manager

To render the website locally, do:

``` bash
$ git clone git@gitlab.com:mlcdresden/pages.git
$ cd pages
$ make prepare
#a lot of output
$ make serve
bundle exec jekyll serve
Configuration file: /some/path/pages/_config.yml
            Source: /some/path/pages
       Destination: /some/path/pages/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 12.315 seconds.
 Auto-regeneration: enabled for '/some/path/pages'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
```

Then open a browser and go to the URL: `http://127.0.0.1:4000`

## How to add posts

The entire website is driven by markdown files. Feel free to study the [documentation](https://kramdown.gettalong.org/quickref.html) about authoring pages with markdown flavor used here.

So in order to contribute a post, create a branch first:

``` bash
$ git checkout -b my-new-post
$ cd _posts
$ ${EDITOROFCHOICE} year-month-day-title.md 
```

Note that the date in the filename  is important. It will be the date on which the post will be published. The markdown file has to have the following front-matter before any real markdown can be put:

``` markdown
---
layout: post
title:  Minutes of the Organisation Team
featured-img: meeting-minutes
---
```

The `layout` key is the most important. It defines the layout of the html page later-on. The `title` key defines what is printed as the blog title. The `featured-img` is optional, it's value defines the file base name of a corresponding image file which will be visible as the post image.

### Adding a featured image

If you want to use a `featured-img`, take a `.jpg` file of your choice. Place this file in the subfolder `_img/posts`. For example, there is `_img/posts/meeting-minutes.jpg` in the aforementioned directory. Currently, only `.jpg` are supported as `featured-img`.

In order to create all required thumbnails from this `featured-img`, go to the command line. Switch to the root directory of the repository and invoke `make images`. This call will use nodejs `gulp` to produce various clones of your image that jekyll requires to put on the website. So, when you call `make images` and `_img/posts/meeting-minutes.jpg` was just added, this gives:

``` shell
$ ls ./assets/img/posts/meeting-minutes*.jpg
./assets/img/posts/meeting-minutes.jpg     ./assets/img/posts/meeting-minutes_placehold.jpg  ./assets/img/posts/meeting-minutes_thumb.jpg
./assets/img/posts/meeting-minutes_lg.jpg  ./assets/img/posts/meeting-minutes_sm.jpg         ./assets/img/posts/meeting-minutes_xs.jpg
./assets/img/posts/meeting-minutes_md.jpg  ./assets/img/posts/meeting-minutes_thumb@2x.jpg
```

The example above is taken from [2018-11-28-orga-meeting.md](_posts/2018-11-28-orga-meeting.md). In case you have any question, feel free to create an issue!

### Images in Posts

If you wish to add images in posts, then put them into `assets`. From within the markdown of a blog post, you can then include the image by doing:

``` markdown
![Catchment Map]({{site.baseurl}}/assets/2019-10-02-map.png)
(source[^source2])
```

# TODOs

- [ ] check build instructions under any windows flavor
- [ ] enable/disable disqus comments
- [ ] research social media coverage
- [ ] replacement for gulp based image resizer

