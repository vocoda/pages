---
layout: post
title:  Minutes of the Organisation Team
featured-img: meeting-minutes
---

## Disclaimer

We will publish the meeting minutes of the organisation committee whenever a meeting occurs. These notes may be in German or English. In case, they are in German, feel free to contact us and get a summary in English.

## Present

Peter Winkler, Patrick Stiller, Falk Zakrzewski, Sunna Torge, Heide Meißner, Steffen Seitz, Peter Steinbach, sowie Andreas Gocht und Sunna Torge beim ersten Teil


## Studentenprojekte

- Diskussion ob und wie die MLC eine Rolle bei der Verteilung / Organisation von Studentenprojekten spielen soll
- Promoten von Themen ist von Chefetage limitiert, Studenten sollen nicht weglaufen
- Themen auf Webseite? Vorstellung der Institute, inklusive Betreuer => Probleme: das muss aktuell gehalten werden und Studenten finden Seite nicht
- Einigung: Verteilung von Themen über die MLC Mailingliste, wenn sich für ein Thema kein Student findet, oder ein Student ein Thema aus einem speziellen Bereich sucht


## Peter Steinbachs Vortrag

- Git Tutorial, open source Lehrmaterial
- interaktiv
- Betreuer: ein paar notwendig, bei 40 Personen: 4. Einer davon: Patrick Stiller
- Raum: HS im Görgesbau, Strom und WLAN => Steffen
- PC-Pool eventuell => WIL oder APB, Nachteil: andere Umgebung
- Laptop mitbringen
- mögliche Termine im Dezember: 17. nachmittags, 18. ganz, 19. vormittag, Do nachmittag, (Freitag ganz)
- Kommandozeilenkenntnisse
- Registrierung, Einladung => Peter Steinbach, wenn Termin und Raum steht
- zusätzlicher WLAN Zugang? => Steffen
- weitere Verteiler nötig? Steffen meldet Bedarf


## Vortrag bei MLDD meetup Gruppe

- Peter W. Hat Anfrage:
- Interesse?
- Sinn, Ziel?
- Peter Steinbach würde


## Weitere Ausrichtung der MLC?

- Richtung Wirtschaft?
- Sponsoring?
- Eisdetektion auf Windkraftanlagen?
- Links auf (veraltete) Webseiten der beteiligten Institute


## Weitere Vorträge?

- Steffen: RNN, Autoencoders => Steffen: möglichen Termin mitteilen
- Amazon, Unit, TUM (VW)
- Steffen: wird Leute ansprechen VW
- Peter: Amazon
Gruppe bei Unit => später

## Workshop nächstes Jahr?

- Datum, Einladung? 
- Zuerst Raum suchen
- der große im ABP => Patrick
- Mitte Mai, evtl. am Dies academicus (22.5.2019)


## Sonstiges:

- neuer ML-Prof, Berufung steht, Name noch nicht bekannt
- IMB: auch neue Gruppe, Juniorprof
- Politik: [Fraunhofer Institut für KI](https://www.cdu-fraktion-sachsen.de/aktuell/pressemitteilungen/meldung/klarer-kurs-fuer-sachsens-wissenschaftslandschaft.html) in Dresden?
