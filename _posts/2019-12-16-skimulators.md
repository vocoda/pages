---
layout: post
title:  The Early Bird catches the best simulation
featured-img: 2019-12-18-Layout-of-SuperKEKB-at-the-KEK-Tsukuba-campus
---
![Aerial view of the SuperKEKB accelerator and it's experiments.]({{site.baseurl}}/assets/img/posts/2019-12-18-Layout-of-SuperKEKB-at-the-KEK-Tsukuba-campus.jpg)

At the [2019 Deep Learning Hackathon](https://indico.mpi-cbg.de/e/d3hack2019) in Dresden, a team led by Andreas Lindner (Ludwig-Maximilian University Munich) proposed to use Deep Learning to increase the possible effective volume of physics simulations for the Belle II experiment. Andreas' team was voted to be one of the most creative at the hackathon. This blog post provides an overview of what the team achieved.

The team consisted of:

    ..* Emilio Dorigatti (LMU Munich) - hackathon mentor
    ..* James Kahn (KIT) - started the project in 2017
    ..* Kilian Lieret (LMU Munich, Excellence Cluster Origins) - experienced programmer
    ..* Andi Lindner (LMU Munich, Excellence Cluster Origins) - joined the project in mid 2019

# The idea

Our project attempts to increase the possible effective volume (with respect to the number of events generated) of physics simulations for the Belle II experiment by predicting early in the simulation procedure whether a simulated collision is useful for a particular physics analysis. This is achieved by a dedicated neural network that is trained on collisions which are known to be useful or not. Once events are classified, computationally expensive simulation steps can then be skipped for those events classified as not useful.

Limited by computing resources, current simulation methods in the particle physics community are too slow for the number of simulated events required in planned physics studies. Most of the simulated physics events are trivially identified after simulation as being not useful for the given study and discarded. This project tries to make this decision as early as possible.

A faster simulation overall results in more simulated events which in turn helps to layout physics studies. For our current analysis, especially hard to observe and rare physics events (observation of the yet unknown and precise measurement of the known), we need a large number simulated events to compare to detector observations. This way known physics effects can be "removed" from the experimental data. Here is our proposed filtering pipeline for simulated events:

![Proposed Filtering of simulated Events With Neural Network]({{site.baseurl}}/assets/2019-12-18-MC_data_flow-keep-discard_NN.jpg)


# The data

At the hackathon we used about three times 300.000 simulated collisions labelled according to three different kinds of selection criteria (for different physics studies). While this is enough for quick trainings and tunings of the network, we have tens of millions of already calssified simulations available for exhaustive evaluations later on.

For each simulated particle collision, we collect some features we think might help the neural network. These include particle properties and the information about the decay chain. 

![Event features used as input to our neural network]({{site.baseurl}}/assets/2019-12-18-input_features.jpg)

All raw data is centrally provided by the data production group of the Belle II collaboration. To prepare training of the neural network, we had to do select datasets, extract and preprocess thereof in order to be useful.

# Hackathon experiences

Our idea for the hackathon was to switch to the relatively new field of graph neural networks. More precisely we were implemented a specialisation of the architectures described in [Xu et al, "How Powerful are Graph Neural Networks?" (2018)](https://arxiv.org/abs/1810.00826). With this change of approach we hoped to be able to take into account the geometry of a particle collision and the subsequent particle decays (which are the actual characteristics) and thus we hoped to improve on results obtained in the past. Here is a typical decay chain we are interested in.

![Schematic Patricle Decay Structure at Belle2]({{site.baseurl}}/assets/2019-12-18-standard_BelleII_decay_fromsvg.png)

When started the hackathon in Dresden, we had an idea and a first simple architecture. Our mentor Emilio Dorigatti had a background in statistics of life science data. He was very adaptive and with his deep knowledge about machine learning, he became the driving force behind reconstructions of our architecture. One has to note, that he also he did not have much experience with graph neural networks. We adpated our architecture from a reference implementation in Tensorflow/Keras taken from [Thomas Kipf's repo](https://github.com/tkipf/keras-gcn).

At the Dresden hackathon we could completely focus on our project for 4 days (neglecting Friday) under constant input from our mentor. Playing around on the first days, we reached AUC scores of `0.6-0.8` which was not satisfying for us. We came to Dresden to tackle 0.9 as James had almost reached 0.8 with his approach using 1D convolutions prior to the workshop.

That was when two crucial structural changes were devised. First, we modified the graph convolutions such that an additional set of weights is learned compared to the original form by [Kipf](http://tkipf.github.io/graph-convolutional-networks/) - we train independent sets of weights for node parents, graph nodes and node children. Second, we combined one of the old architectures of 1D convolutions with the new graph convolutions into a parallel architecture.

![Schematic Network Architecture for our GraphNN]({{site.baseurl}}/assets/2019-12-18-kahn_CHEP19_KIT_p9.png)

# Results

With the improved architecture we achieved AUC score of `0.94-0.98`! The performance is definitely superior to what we had before and would not have been possible to achieve without the hackathon (at least certainly not within 4 days!). 

Our [work was presented by James](https://indico.cern.ch/event/773049/contributions/3474758/attachments/1937900/3212101/CHEP19_KIT.pdf) at the [CHEP 2019 conference](https://indico.cern.ch/event/773049) and a subsequent paper is currently being written. Reduction of computing resoures is a big topic everywhere around particle physics, so we feel that this was a very important contribution.

This article was written by Andreas Lindner (LMU) and Peter Steinbach (HZDR).
