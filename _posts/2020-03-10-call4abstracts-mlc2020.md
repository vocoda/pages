---
layout: post
title:  Third Annual MLC Workshop
featured-img: 2020-03-10-call4abstracts
---

Dear Machine Learning Enthusiasts and Practitioners,

On behalf of the “Machine Learning Community (MLC)”, we cordially invite you to join our [3rd Annual MLC Workshop](https://hifis-events.hzdr.de/event/7/) from May 18 to May 19, 2020, in Dresden. As every year, the overall aim of our get-together is: 

- finding like-minded practitioners and experts 
- discussing problems
- exchanging solutions
- getting inspiration from other ML users!

In the tradition of this spirit, you are invited to send abstracts on your recent work involving the application of machine learning on a specific research topic. To group similar topics, the abstracts will be peer-reviewed prior to acceptance or rejection. We encourage submission of work-in-progress subjects that you want feedback on as well as completed projects which have possibly already been published.

Please submit your abstract for a contribution to [our call for abstracts](https://hifis-events.hzdr.de/event/7/abstracts/) by April 10, 2020. The workshop venue is the "Festsaal der Wirtschaftswissenschaften", Hülße-Bau on the TU Dresden main campus. 

The event is generously supported by 
- the Data Science competence centers [CASUS](https://www.hzdr.de/db/Cms?pOid=57317&pNid=1227) 
- the Center for Scalable Data Analytics and Artificial Intelligence [SCADS](https://www.scads.de/en/)
- [Helmholtz AI](https://helmholtz.ai) Local Unit at [HZDR](https://hzdr.de)
- Helmholtz Federated IT services [HIFIS](https://www.hifis.net)

Because of this support we are very happy to announce the following highlight talks:

1. [Prof. Björn Andres](http://www.andres.sc/), Professor of Machine Learning for Computer Vision at TU Dresden

2. [Prof. Nikos Psarros](http://psarros.info/), Professor of Philosophy at University of Leipzig

In case of any further questions, don’t hesitate to contact our team members! If you'd like to follow our workshop on twitter.com, please use [#mlcdd2020](https://twitter.com/hashtag/mlcdd2020).

We look forward to see you at the Workshop, the MLC organising team!

Jeffrey Kelling (HZDR), Heide Meißner (HZDR), Steffen Seitz (TU Dresden), Peter Steinbach (HZDR), Peter Winkler (TU Dresden)

&nbsp;
&nbsp;

PS. If you'd like to help organizing or support our workshop, please let us know. We are mostly volunteers under the good will of our employers. We are always looking for helping hands.
