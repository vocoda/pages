---
layout: post
title:  Minutes of the Organisation Team
featured-img: meeting-minutes
---

## Disclaimer

We will publish the meeting minutes of the organisation committee whenever a
meeting occurs. These notes may be in German or English. In case, they are in
German, feel free to contact us and get a summary in English.

## Present

Peter Winkler, Heide Meißner, Peter Steinbach, 	Dannell Quesada Chacon, Jeffrey Kelling 

## MLC Workshop
* will probably have to be postponed
* decision on postponing will be taken as scheduled on 14th of April
* try to stay in the same room to reduce overhead
* possible date: first week of October, if the winter semester is likely to not move
  * TUD summer term delayed by one month

## MLC DL Link List
* relates to [issue 18](https://gitlab.com/mlcdresden/pages/-/issues/18)
* create a PR against website for the list: [see comment](https://gitlab.com/mlcdresden/pages/-/issues/18#note_314686131)
