---
layout: post
title:  Jan 2021 Seminar Recap
featured-img: 2021-01-12-pexels-anna-tarazevich-5425647-done
---

The talk by [Oliver Guhr]({{site.baseurl}}/posts/) was very well attended by 43 participants. Thank you again to Oliver to deliver such a wonderful description of Transformers and a step-by-step discussion of the attention mechanism.

## Poll Recap

To capture the community and learn of their needs, we conducted two polls with all logged in users. Each poll asked participants a question to which they were meant to pick one single answer.

### What is the primary data type that you use ML for?

| votes | answer                        |
|:-----:|-------------------------------|
| 40% | images                        |
| 10% | text                          |
| 10% | video                         |
| 10% | 3D volumes (voxel based data) |
|  7% | graphs                        |
|  4% | 3D volumes over time          |
| 25% | other                         |

It is interesting but perhaps unsurprising to see that most people appear to use machine learning (ML) for images (`40%`). An astonishing amount of `25%` couldn't find a description of their dataset in the allowed answers.

### How much hardware do you use for (training) machine learning?

| votes | answer                                    |
|:-----:|-------------------------------------------|
| 7%    | one computer (worksation, laptop, server) |
| 31%   | one computer with a GPU inside            |
| 13%   | one computer with multiple GPUs inside    |
| 4%    | multiple computers (without GPU usage)    |
| 19%   | multiple computers with one GPU each      |
| 31%   | multiple computers with multiple GPU each |


Here again, the results share something unsurprising: `50%` of our attendees use some kind of HPC or cloud infrastructure as they use __multiple computers__ at the same time __with one GPU each__. It is interesting to see that almost a comparable ration of attendees do not do so, `44%` answered that they use one computer with at least 1 GPU inside .

## Seminar Notes

To make the seminar more interactive for everyone, we set up interactives notes. We used them to connect to eachother, submit questions, add further material for the talk. These notes with additional links and resources are available for download as [markdown formatted text file]({{site.baseurl}}/assets/20210112-seminar-oliverguhr-transformers.md).

## Slides

Slides are available as pdf file at [here](https://cloudstore.zih.tu-dresden.de/index.php/s/8YSAojinga99kkW).
