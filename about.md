---
layout: page
title: About
permalink: /about/
---

We are a grass-roots movement of Deep Learning practitioners in Dresden, Germany.

## Members of the MLC Organizing Committee

* Dánnell Quesada Chacón
  ([TU Dresden](https://tu-dresden.de/))
* Heide Meißner
  ([HZDR](https://www.hzdr.de))
* Jeffrey Kelling
  ([HZDR](https://www.hzdr.de))
* Vincent Latzko
  ([TU Dresden](https://tu-dresden.de/))
* Peter Steinbach
  ([MPI-CBG](https://www.mpi-cbg.de), [Scionics](https://www.scionics.de))
* Peter Winkler
  ([ScaDS](https://www.scads.de/), [TU Dresden](https://tu-dresden.de/))
* Steffen Seitz
  ([TU Dresden](https://tu-dresden.de/))

### Former Members of the MLC Organizing Committee

* Patrick Stiller
  ([HZDR](https://www.hzdr.de))
* Matthias Werner 
  ([HZDR](https://www.hzdr.de))
* Falk Zakrzewski
  ([UKD](https://www.uniklinikum-dresden.de/en))

[![HZDR](/pages/assets/img/logos/hzdr.png)](https://www.hzdr.de)  [![TU Dresden](/pages/assets/img/logos/tud.png)](https://tu-dresden.de/)  [![ScaDS](/pages/assets/img/logos/scads.png)](https://www.scads.de/)  [![MPI-CBG](/pages/assets/img/logos/mpicbg.png)](https://www.mpi-cbg.de)
