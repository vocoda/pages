# MLC Seminar Jan 21, 2021: "Transformers" by Oliver Guhr

This hackmd document will serve as the show notes for Oliver Guhr's talk. Please use it for questions or important notes that add to the content of the speaker. After the talk, these notes will be stored and made publicly on the seminar website https://mlcdresden.gitlab.io/pages/2020/12/10/seminar20210112.html

For the entire seminar, please be mindful of your peers and supportive in your communication. We henceforth make following the Dresden code of conduct obligatory for everyone connected:
https://dresden-code-of-conduct.org/de/
In light of this, please mute yourself if you are not talking!

## Roll Card / Participants

Please consider registering any contact details or your name in this document. This will help participants to connect to you after the seminar. What contact details you provide is up to you.

- Peter Steinbach (HZDR), github: @psteinb , twitter: @psteinb_


## Agenda

1. Introduction by Peter Steinbach (HZDR), 10 minutes
2. Transformers by Oliver Guhr (HTW Dresden), 35-45 minutes

**[Link to the Slides](https://www.dropbox.com/s/mfa1nxpjiia4v70/MLC-Transformer%20and%20Attention-2021.pdf?dl=0)**

Abstract:
> Deep Neural Networks have revolutionized image processing, but these successes could not simply be transferred to text processing - until 2017 when the Transformers were introduced. This new architecture has brought great advances in many areas of speech processing, and some, such as generating compelling text, are now possible. Together we look at how transfer learning and attention work with the Transformer architecture.

3. Feedback by Peter Steinbach (HZDR), 5 minutes

## Results of the Poll

35 people were signed in that we presented with the poll questions. 33 participated.

### What is the primary data type that you use ML for?

- `40%` images
- `10%` text
- `10%` video
- `10%` 3D volumes (voxel based data)
- ` 7%` graphs
- ` 4%` 3D volumes over time
- `25%` other

### How much hardware do you use for (training) machine learning?

- ` 7%` one computer (worksation, laptop, server)
- `31%` one computer with a GPU inside
- `13%` one computer with multiple GPUs inside
- ` 4%` multiple computers (without GPU usage)
- `19%` multiple computers with one GPU each
- `31%` multiple computers with multiple GPU each

# Notes and Questions for the Talk

- related to the poll: what "other" datatypes do people use for ML?
  - 1D sequences (200 float32 values each) of a beamline profile 


- attention is all you need: https://arxiv.org/abs/1706.03762
- the illustrated BERT, http://jalammar.github.io/illustrated-bert/
- Oliver's experiments with transformers to predict time series 
https://github.com/oliverguhr/transformer-time-series-prediction
- Motivation to attend the talk:
    - get some background information on Transformers
    - learn about transformers for image analysis
    - background on Transformers, never found the time to read the papers since that is not my area 
      of application, so thanks :) | +1

# Feedback

## Share with us something that you liked :+1: or learned during the talk :heavy_check_mark: 

- transformers do a lot of MM operations or dotproducts in that sense
- "the bigger the models, the more capacity the prediction has" This appears to apply to language model to equal extent as to image based models
    - The GPT-3 paper is an intresting read in this regard. It shows that many tasks (like translation) can be sloved without task specific data - just by a much bigger model.
    - GPT-3: [Language Models are Few-Shot Learners](https://arxiv.org/abs/2005.14165)
- Could you please, also provide the references to the work that tried to diminish the computational costs by adapting the attention layer
    - This is a very active field - there are probably more papers on that topic. Here are a few I am aware of, but I never tried them.
    - [Reformer: The Efficient Transformer](https://arxiv.org/abs/2001.04451)
    - [Longformer: The Long-Document Transformer](https://arxiv.org/abs/2004.05150)
    - [Linformer: Self-Attention with Linear Complexity](https://arxiv.org/abs/2006.04768)

## Share with us something that you didn't understand :-1: or want the speaker to improve :question: 

- I lost track in the middle sometimes when slides were being flipped back and forth (caught up later)
    - I am sorry - just as I was doning this I thought that this was a bad idea. So thank you for the feedback, I will fix this.
- Sorry, I missed how to get the references on the slides
    - You can find [the slides here](https://www.dropbox.com/s/mfa1nxpjiia4v70/MLC-Transformer%20and%20Attention-2021.pdf?dl=0)
    - (The PDF with the links will be provided, afaik)