#!/bin/bash

# scrape email adresses from mailman for move to new list

COOKIE=$1
if [ -z "$COOKIE" ]; then
	echo "need session cookie: '<name>=<token>'"
	exit 1
fi
LIST=$2
if [ -z "$LIST" ]; then
	LIST="zih-mlc"
fi

URL="https://mailman.zih.tu-dresden.de/groups/admin/zih-mlc/members?letter="
URL="$( echo "$URL" | sed "s/zih-mlc/$LIST/" )"

for i in {a..z}; do
	wget --header "Cookie: $COOKIE" $URL$i -O - 2>/dev/null | sed -n 's/.*value="\(.*%40.*\)".*/\1/p' | sed 's/%40/@/' | sort -u
done
